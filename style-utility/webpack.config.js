const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa");
const path = require('path');

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "tuwien",
    projectName: "style-utility",
    webpackConfigEnv,
    argv,
  });

  return merge(defaultConfig, {
    output: {
      libraryTarget: "system"
    },
    module: {
      rules: [
        {
          test: /\.css$/i,
          include: path.resolve(__dirname, 'src'),
          use: ['postcss-loader'],
        },
      ],
    },
    // modify the webpack config however you'd like to by adding to this object
  });
};
